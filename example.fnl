#!/usr/bin/env fennel

(local thrud (require :thor))
;;(local thrud (require :frey))



;;#+title:  Example Org File
;;#+author: TEC
;;#+date:   2020-10-27
(local properties (thrud.metadata "example org file" "Erik Lundstedt" "2022-05-23"))


(local someInfo
       (table.concat [
       (thrud.header 2 "here is a headder")
       (table.concat
        (thrud.freetext
         [
          "here is some text"
          "here is some more text"
          (.. "here is some " (thrud.bold "bold") "text")
          (.. "here is some " (thrud.slanting "slanting") "text")
          ]
         )
        )
        ])
       )

(local todolist
       [
        {:text "take a walk" :state false}
        {:text "check mail" :state true}
        ])

(local lvl3
       {
        :level 3
        :title "even more things to do"
        :content
        [
         {:text "foo" :state true }
         {:text "bar" :state true }
         {:text "baz" :state false }
         ]
        }
       )

(local lvl2
       {
        :level 2
        :title "things to do"
        :content
        [
         {:text "foo" :state true }
         {:text "bar" :state false }
;;         (thrud.checklist lvl3)
         {:text "baz" :state false }
         ]
        }
       )


(local numlist
       [
        "hello"
        "world"
        "this"
        "is"
        "a"
        (thrud.bold "numeric")
        (thrud.slanting "list")
        ])




(local note
       (thrud.section
        (thrud.header 1 "this is a document for testing")
        [
         someInfo
         ;; properties
         ;;(thrud.numericList numlist)
         ;;(thrud.checklist todolist)
         (thrud.checklist lvl2)
         ]
        )
       )



;;(sif.preview note)
;;(sif.writeFile note "example.org" )

note
