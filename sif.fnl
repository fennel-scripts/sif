#!/usr/bin/env fennel
(local sif {})



(fn sif.preview [content]
  (print content)
)


(fn sif.writeFile [content filename]
  (with-open [fout (io.open filename :w) ]
	(fout:write content)
	)
  (print (.. "wrote " "data " "to " filename))
  )

sif
