#!/usr/bin/env fennel

(local sif (require :sif))
(local thor (require :thor))
(local doc [])




(local documents
       {
;;        :name {:in (require :name) :out "name.org"}
        ;;:readme  {:in (require :readme)  :out "readme.org"}
        :example {:in (require :example) :out "example.org"}
        }
       )
(table.insert doc (thor.header 1 "documentation"))
(table.insert doc (thor.header 2 "preview content"))

(each [k v (pairs documents)]
  (sif.preview v.in)
  )

(table.insert doc (thor.header 2 "write content documents"))
(each [k v (pairs documents)]
  (sif.writeFile v.in v.out)
  )

(sif.preview (thor.section " " doc))
