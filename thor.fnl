#!/usr/bin/env fennel

(local thor {})


(fn thor.prop [property]
  (local name (or property.name ""))
  (local value (or property.value ""))
  (.. "#+" name ": " value)
  )

(fn thor.metadata [title author date]
  (table.concat
  [
		(thor.prop {:name "title"  :value title})
		(thor.prop {:name "author" :value author})
		(thor.prop {:name "date"   :value date})
		]
  "\n")
  )





(fn indent [level char]
  (local retbl [])
  (for [i 1 level]
	(table.insert retbl char)
	)
  (table.concat retbl)
  )

(fn iflen [k len]
  (if (= k len)
	  ""
	  "\n"
	  )
  )







(fn thor.header [level text]
  (var headGen "")
  (for [i 1 level]
	(set headGen (.. headGen "*"))
	)
  (.. headGen " " text "\n")
  )

;; (fn thor.checkbox [state text]
;;   (.. "- [" (if state "X" " ") "] " text )
;;   )

;; (fn thor.checklist [items]
;;   (local retval [])
;;   (each [key value (pairs items)]
;;     (table.insert retval (thor.checkbox value.state value.text))
;;     )
;;   (table.concat retval "\n")
;;   )

(fn thor.numericList [items]

(fn numericItem [number text]
  (.. number ". " text )
  )

  (local retval [])
  (each [i v (ipairs items)]
	(table.insert retval (.. "\t" (numericItem i v)))
	)
  (table.concat retval "\n")
  )



(fn thor.checklist [list]
  (local retbl [])
  (local level list.level)
;;  (if (= level 1)
;;      (table.insert retbl "* ")
;;      )
  ;; (table.insert retbl (.. list.title) "\n")

  (table.insert retbl (thor.header level list.title ))
  ;;insert the name/title into the list that gets returned later
  (each [k v (ipairs list.content)]
	;;for-each loop, loops thru the table at index "content" of the input-table
	(table.insert retbl
	 (.. (indent level " ")
		 (.. "- [" (if v.state "X" " ") "] " v.text )
		 (iflen k (length list.content))
		 )
	 )
	;;insert the items and some styling in the table we return later
	;; also make sure to format correctly depending on if it is a "sublist"(the indentation  level is more than 2)
	);;this ends the for(each) loop
  (table.concat retbl)
  )







(fn thor.freetext [lines]
  [(table.concat lines "\n")]
  )


(fn thor.drawer [name lines]
  [
   (.. ":" (string.upper name) ":")
   (table.concat lines "\n")
   ":END:"
   ]
  )




(fn thor.section [header items]
  (table.concat
   [
	(.. header "\n")
	(table.concat items "\n")
	]
   )
  )



(fn thor.slanting [text]
  (.. "/" text "/ ")
  )
(fn thor.bold [text]
  (.. "*" text "* ")
  )





(fn thor.note [structure]
  (local meta structure.meta)
  (local sections structure.sections)
  )



thor
